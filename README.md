# Zip Predictor

An experiment with machine learning to see if we can predict when the bot should send the "send a diagnostic zip"
message.

# Uses

This project has been built using:

- ElasticSearch (*Ketkev wanted to get some practice*)
- scikit-learn
- Spacey
- Jupyter notebook

# Usage

To get the necessary dependencies running, start the docker-compose with `docker-compose up`. This will prepare the
jupyter notebook server and the elasticsearch server. While starting the containers, the url and token for the jupyter
notebook server can be seen in the output.

When running this for the first time, you have to fill the elasticsearch database with the messages first. You can use
the `data_to_es` notebook for this.

# Gitiquette

To avoid headache inducing merge conflicts, please please please clear your output before committing.