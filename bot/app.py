import atexit

import discord

from client import AutoPilot
from commands import Commands, CommandErrHandler
from config import Config
from model import Model


def cleanup(settings: Config):
    print("Cleaning up")
    settings.save()


if __name__ == "__main__":
    config = Config()
    atexit.register(cleanup, config)

    model = Model(config)

    intents = discord.Intents.default()

    client = AutoPilot(
        command_prefix='$',
        intents=intents,
        model=model,
        config=config
    )

    client.add_cog(Commands(client))
    client.add_cog(CommandErrHandler(client))

    client.run(config.token)
