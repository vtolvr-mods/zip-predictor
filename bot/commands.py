import sys
import traceback

import discord
from discord import TextChannel
from discord import User
from discord.ext import commands
from discord.ext.commands.context import Context

from client import AutoPilot


class Commands(commands.Cog):
    def __init__(self, bot: AutoPilot):
        self.bot = bot

    @commands.command()
    async def set_output_channel(self, ctx: Context, channel: TextChannel):
        """ Sets where to output debug information """
        self.bot.config.output_channel = channel.id

    @commands.command()
    async def add_channel_to_watch(self, ctx: Context, channel: TextChannel):
        """ Add a channel to the channels to watch for help """
        if channel.id not in self.bot.config.channels_to_watch:
            self.bot.config.channels_to_watch.append(channel.id)

    @commands.command()
    async def remove_channel_to_watch(self, ctx: Context, channel: TextChannel):
        """ Remove a channel from the channels to watch for help """
        if channel.id in self.bot.config.channels_to_watch:
            self.bot.config.channels_to_watch.remove(channel.id)

    @commands.command()
    async def add_ignored_user(self, ctx: Context, user: User):
        """ Add a user to the ignored list"""
        if user.id not in self.bot.config.ignored_users:
            self.bot.config.ignored_users.append(user.id)

    @commands.command()
    async def remove_ignored_user(self, ctx: Context, user: User):
        """ Remove a user from the ignored list """
        if user.id in self.bot.config.ignored_users:
            self.bot.config.ignored_users.remove(user.id)

    @commands.command()
    async def set_send_help_message(self, ctx: Context, send_help: bool):
        """ Set whether to send a help message """
        self.bot.config.send_help_message = send_help

    @commands.command()
    async def set_min_conf_to_log(self, ctx: Context, confidence: float):
        """ Set the confidence required to log a help message """
        self.bot.config.minimum_confidence_to_log = confidence

    @commands.command()
    async def set_min_conf_to_send(self, ctx: Context, confidence: float):
        """ Set the confidence required to send a help message """
        self.bot.config.minimum_confidence_to_send = confidence

    @commands.command()
    async def set_min_word_count(self, ctx: Context, count: int):
        """ Set the word count required to run a prediction """
        self.bot.config.minimum_word_count = count

    @commands.command()
    async def get_config(self, ctx: Context):
        """ Print a censored config """
        await ctx.send(self.bot.config.get_clean_copy())


class CommandErrHandler(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        """The event triggered when an error is raised while invoking a command.
        Parameters
        ------------
        ctx: commands.Context
            The context used for command invocation.
        error: commands.CommandError
            The Exception raised.
        """
        if isinstance(error, discord.ext.commands.CommandNotFound):
            print(f'Command not found: {error}')
        else:
            print('Ignoring exception in command {}:'.format(
                ctx.command), file=sys.stderr)
            traceback.print_exception(
                type(error), error, error.__traceback__, file=sys.stderr)
