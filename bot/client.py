from discord import Message
from discord.ext import commands

from config import Config
from model import Model


def sent_by_admin(message: Message) -> bool:
    role_list = [
        "Pilot",
        "CoPilots"
    ]

    return any(role.name in role_list for role in message.author.roles)


class AutoPilot(commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model: Model = kwargs.pop('model')
        self.config: Config = kwargs.pop('config')

    async def on_ready(self):
        print(f'Logged on as {self.user.name}!')

    async def on_message(self, message: Message):
        if message.author == self.user:
            return

        await self.try_helping(message)

        if sent_by_admin(message):
            await self.process_commands(message)
            self.config.save()

    async def try_helping(self, message: Message):
        if self.config.channels_to_watch and message.channel.id not in self.config.channels_to_watch:
            return
        if message.author.id in self.config.ignored_users:
            return

        response = self.model.handle_message(message)

        reply_message = f'''Wanted to help user here:

{message.jump_url}

based on context: "{response.context}"

False: {response.false_confidence}
True: {response.true_confidence}
'''
        if response.needs_help and response.true_confidence >= self.config.minimum_confidence_to_log:
            await self.get_channel(self.config.output_channel).send(reply_message)

        if self.config.send_help_message and response.needs_help and response.true_confidence >= self.config.minimum_confidence_to_send:
            await message.channel.send("/r zip")
