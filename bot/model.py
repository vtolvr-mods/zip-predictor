import pickle
from dataclasses import dataclass

from discord import Message
from discord import TextChannel

from config import Config


@dataclass
class Response:
    needs_help: bool = False
    false_confidence: float = 0.0
    true_confidence: float = 0.0
    message: Message = None
    context: str = ""


def count_words(message: str) -> int:
    words = [word for word in message.split(' ') if word != ""]
    return len(words)


class Model:
    def __init__(self, config: Config):
        self.__config = config
        self.__messages: dict[TextChannel, list[Message]] = {}
        with open('model.pickle', 'rb') as file:
            self.__model = pickle.load(file)

    def handle_message(self, message: Message) -> Response:
        if message.channel not in self.__messages:
            self.__messages[message.channel] = []

        self.__messages[message.channel].append(message)

        # Remove messages older than 5 minutes
        self.__messages[message.channel] = [
            m for m in self.__messages[message.channel] if (message.created_at - m.created_at).seconds < 300
        ]

        messages = ' '.join(
            [m.content for m in self.__messages[message.channel]])

        if count_words(messages) < self.__config.minimum_word_count:
            return Response(False)

        prediction = self.__model.predict([messages])[0]
        confidence = self.__model.predict_proba([messages])[0]

        if prediction:
            self.__messages[message.channel] = []

        return Response(prediction, confidence[0], confidence[1], message, messages)
