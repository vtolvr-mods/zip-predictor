import json
from json import JSONDecodeError

ConfigFileName = "config.json"


class Config:
    def __init__(self):
        # TODO: Fix names
        self.token: str = ""
        self.channels_to_watch: list[int] = []
        self.output_channel: int = 0
        self.send_help_message: bool = False
        self.ignored_users: list[int] = []
        self.minimum_confidence_to_log: float = 0.0
        self.minimum_confidence_to_send: float = 0.0
        self.minimum_word_count: int = 0

        try:
            with open(ConfigFileName, 'r') as file:
                config = json.load(file)
        except (FileNotFoundError, JSONDecodeError):
            print("Can't find", ConfigFileName)
            print("Creating default settings file and exiting")
            self.save()
            exit(2)

        for key in config:
            if key in self.__dict__:
                self.__dict__[key] = config[key]

    def save(self):
        with open(ConfigFileName, 'w') as file:
            json.dump(self.__dict__, file, indent=2)

    def get_clean_copy(self) -> str:
        cpy = self.__dict__.copy()
        cpy["token"] = "[REMOVED]"

        return json.dumps(cpy, indent=2)
